# Setup

## set the right node config
``` shell
nvm install 6.11.0
nvm alias default 6.11.0
nvm version
```

## install dependencies
``` shell
cd webapp
npm install
npm run dev
```
